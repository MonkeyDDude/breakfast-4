<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Participant;
use AppBundle\Entity\VacationSlot;
use AppBundle\Form\Type\BreakfastDoneType;
use AppBundle\Form\Type\ParticipantType;
use AppBundle\Form\Type\VacationSlotType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_home")
     *
     * @return Response
     */
    public function homeAction()
    {
        return $this->render(':admin:home.html.twig');
    }

    /**
     * @Route("/participants", name="admin_participants")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function participantsListAction(Request $request)
    {
        $sort = $request->query->has('date') ?
            ['lastParticipatedAt' => $request->query->get('date') === 'DESC' ? 'DESC' : 'ASC'] :
            ['name' => 'ASC'];

        $participants = $this->getDoctrine()->getRepository('AppBundle:Participant')->findBy([], $sort);

        return $this->render(':admin/participants:list.html.twig', ['participants' => $participants]);
    }

    /**
     * @Route("/participants/new", name="admin_participants_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function participantsNewAction(Request $request)
    {
        $participant = new Participant();
        $form = $this->createForm(ParticipantType::class, $participant);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_participants'));
        }

        return $this->render(':admin/participants:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/participants/edit/{id}", name="admin_participants_edit")
     *
     * @ParamConverter("participant", class="AppBundle:Participant")
     *
     * @param Participant $participant
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function participantsEditAction(Participant $participant, Request $request)
    {
        $form = $this->createForm(ParticipantType::class, $participant);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_participants'));
        }

        return $this->render(':admin/participants:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/participants/delete/{id}", name="admin_participants_delete")
     *
     * @ParamConverter("participant", class="AppBundle:Participant")
     *
     * @param Participant $participant
     *
     * @return RedirectResponse
     */
    public function participantsDeleteAction(Participant $participant)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($participant);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_participants'));
    }

    /**
     * @Route("/vacation-slots", name="admin_vacation_slots")
     *
     * @return Response
     */
    public function vacationSlotsListAction()
    {
        $slots = $this->getDoctrine()->getRepository('AppBundle:VacationSlot')->findBy([], ['at' => 'DESC']);

        return $this->render(':admin/slots:list.html.twig', ['slots' => $slots]);
    }

    /**
     * @Route("/vacation-slots/new", name="admin_vacation_slots_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function vacationSlotsNewAction(Request $request)
    {
        $slot = new VacationSlot();
        $form = $this->createForm(VacationSlotType::class, $slot);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($slot);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_vacation_slots'));
        }

        return $this->render(':admin/slots:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/vacation-slots/delete/{id}", name="admin_vacation_slots_delete")
     *
     * @ParamConverter("slot", class="AppBundle:VacationSlot")
     *
     * @param VacationSlot $slot
     *
     * @return RedirectResponse
     */
    public function vacationSlotsDeleteAction(VacationSlot $slot)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($slot);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_vacation_slots'));
    }

    /**
     * @Route("/breakfast-done", name="admin_breakfast_done")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function breakfastDoneAction(Request $request)
    {
        $form = $this->createForm(BreakfastDoneType::class);

        $nextParticipants = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Participant')
            ->getNextParticipants(new \DateTime('next friday'));
        if (count($nextParticipants) === 2 && $request->getMethod() !== Request::METHOD_POST) {
            $form->get('participant1')->setData($nextParticipants[0]);
            $form->get('participant2')->setData($nextParticipants[1]);
        }
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $participant1 = $form->get('participant1')->getData();
            $participant2 = $form->get('participant2')->getData();
            $at = $form->get('at')->getData();
            $participant1->setLastParticipatedAt($at);
            $participant2->setLastParticipatedAt($at);
            $em->merge($participant1);
            $em->merge($participant2);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_home'));
        }

        return $this->render(':admin:new_breakfast_done.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
