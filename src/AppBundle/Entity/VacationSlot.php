<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class VacationSlot
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Participant
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Participant", inversedBy="vacationSlots")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $participant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $at;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param Participant $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }

    /**
     * @return \DateTime
     */
    public function getAt()
    {
        return $this->at;
    }

    /**
     * @param \DateTime $at
     */
    public function setAt($at)
    {
        $this->at = $at;
    }
}
